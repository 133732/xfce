#!/bin/bash
# save current dir
dir=$(pwd)
# zipping: .config, .themes, .icons
cd ~

zip -r config.zip ./.config
zip -r themes.zip ./.themes
zip -r icons.zip ./.icons

# move to backup dir
mkdir backup
mv config.zip ./backup
mv themes.zip ./backup
mv icons.zip ./backup

# zip the backup dir
zip -r backup.zip ./backup
rm -rf ./backup
mv backup.zip $dir
echo "Das Backup wurde erstellt und liegt in" $dir " !"

#!/bin/bash
# install Applications
sudo pacman -S alacritty git zip unzip vim
# create dir for themes
mkdir ~/.icons
mkdir ~/.themes

# unzip backup
unzip backup.zip
echo "Backup wirde entpackt"

# unzip: .config, .themes, .incons
cd backup
unzip config.zip
unzip themes.zip
unzip icons.zip

# move configs...
cp -r .config/* ~/.config/
cp -r .themes/* ~/.themes/
cp -r .icons/* ~/.icons/

echo "Alle Daten wurden kopiert, bitte reboot durchführen!"
